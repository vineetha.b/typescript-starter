const perfect = (x: number) => {
    let sum = 0;
    for (let i = 2; i < x; i++) {
      if (x % i === 0) {
        sum = sum + i;
      }
      return sum;
    }
    if (sum === x) {
      console.log('num is prefect');
    }
  }
  export default perfect;