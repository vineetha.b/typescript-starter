import isEven from './isEven';
test('isEven',()=>{
  expect(isEven(5)).toBeFalsy()
  expect(isEven(11)).toBeFalsy()
  expect(isEven(21)).toBeFalsy()
  expect(isEven(4)).toBeTruthy()
  expect(isEven(16)).toBeTruthy()
})
 