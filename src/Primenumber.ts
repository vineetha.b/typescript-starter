const range = (start: number, stop: number):number[] => {
  const arr:number[] = [];
  for (let i = start; i <= stop; i++) {
    arr.push(i);
  }
  return arr;
};

const Prime=(n:number):boolean=>{
  if(n===1){
    return false;
  }
  if(n===2){
    return true;
  }
  for(let i=2;i<n;i++){
    if(n%i===0){
      return false;
    }
  }
 return true;
}

const Primenumber=(start:number,stop:number):number[]=>range(start,stop).filter(Prime);

export {range,Prime,Primenumber} ;