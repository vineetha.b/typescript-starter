const squareAll = (arr: number[]):number[] => {
    const ar = [];
     for (let i = 0; i < arr.length; i++) {
      ar.push(arr[i] * arr[i]);
    }
    return ar;
  }
  export default squareAll;