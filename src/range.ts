const range = (start: number, stop: number):number[] => {
  const arr:number[] = [];
  for (let i = start; i <= stop; i++) {
    arr.push(i);
  }
  return arr;
};
export default range;
