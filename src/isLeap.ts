const isLeap=(n:number)=>{
  return (n%4===0&&n%400===0&&n%100!==0);
  }
  export default isLeap;