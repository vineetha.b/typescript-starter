import sort from './sort';
test('sort',()=>{
  expect(sort([7,9])).toEqual([7,9])
  expect(sort([11,9])).toEqual([9,11])
  expect(sort([9,12,10])).toEqual([9,10,12])
 
})