import allEven from './allEven';
test('allEven',()=>{
  expect(allEven([2,3,4,5,6])).toEqual([2,4,6]);
  expect(allEven([11,6,8])).toEqual([6,8]);
  expect(allEven([1,9,5,6,7,8])).toEqual([6,8,9]);
expect(allEven([14,9,5,6])).toEqual([14]);
expect(allEven([1,2,3,4,5,6,7,8,9])).toEqual([1,2,4,6,8]);
})