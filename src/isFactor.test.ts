import isFactor from './isFactor';
test('isFactor',()=>{
  expect(isFactor((4),(2))).toBeFalsy();
  expect(isFactor((4),(2))).toBeTruthy();
  expect(isFactor((4),(3))).toBeFalsy();
  expect(isFactor((18),(9))).toBeFalsy();
  expect(isFactor((4),(21))).toBeFalsy();
})