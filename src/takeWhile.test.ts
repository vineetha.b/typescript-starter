import takeWhile from './takeWhile';
test('takeWhile',()=>{
  expect(takeWhile([1,2,3,4],x=>x%2===0)).toEqual([]);
  expect(takeWhile([2,3,4,5],x=>x%2===0)).toEqual([2]);
});