import indexOf from './indexOf';
test('indexOf',()=>{
  expect(indexOf([1,2,3],3)).toEqual(2)
  expect(indexOf([1,6,7,3],3)).toEqual(2)
  expect(indexOf([1,2,13],3)).toEqual(2)
  expect(indexOf([1,2,3],3)).toEqual(3)
  expect(indexOf([1,2,8,9,3],3)).toEqual(2)
})