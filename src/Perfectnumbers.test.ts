import {Perfectnumbers} from './Perfectnumbers';
test('Perfectnumbers',()=>{
  expect(Perfectnumbers(1,10)).toEqual([6]);
  expect(Perfectnumbers(1,30)).toEqual([6,28]);
  expect(Perfectnumbers(1,90)).toEqual([6,28]);
  expect(Perfectnumbers(1,100)).toEqual([6,28]);
  expect(Perfectnumbers(100,500)).toEqual([496]);
})