const isOdd=(s:number)=>{
   return (s%2!==0);
  }
  export default isOdd;