import max from './max';
test('max',()=>{
  expect(max(3,8)).toEqual(8)
  expect(max(99,73)).toEqual(8)
  expect(max(3,5)).toEqual(5)
  expect(max(32,8)).toEqual(32)
  expect(max(35,8)).toEqual(35)
}) 