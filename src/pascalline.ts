const factorial = (n: number) => {
  let fact = 1;
  for (let i = 1; i <= n; i++) {
    fact = fact * i;
  }
  return fact;
};


const ncr = (n: number, r: number): number => {
  return factorial(n) / (factorial(r) * factorial(n - r));
}
const pascalline=(n:number):number[]=>range(0,n+1).map(r=>ncr(n,r));

const pascalTriangle=(lines:number):ReadonlyArray<ReadonlyArray<number>>=>range(0,lines+1).map(n=>pascalline(n));
console.log(pascalTriangle(5));