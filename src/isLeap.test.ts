import isLeap from './isLeap';
test('isLeap',()=>{
  expect(isLeap(2008)).toBeFalsy()
  expect(isLeap(2088)).toBeFalsy()
  expect(isLeap(2014)).toBeFalsy()
  expect(isLeap(3000)).toBeFalsy()
  expect(isLeap(2019)).toBeTruthy()
})