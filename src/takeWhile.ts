const takeWhile=(arr: ReadonlyArray<number>, f: (x: number) => boolean): ReadonlyArray<number>=>{
    const ar=[];
    for(const i of arr)
  {
    if(f(i)){
  ar.push(i);
    }
    else{
     break;
  }
  }
  return ar;
  }
  export default takeWhile;