const gcd=function(x:number,y:number){
     if(!y){
       return x;
     }
     return gcd(y,x%y);
  };
  class Rational{
    readonly numerator:number;
    readonly denominator:number;
    constructor (numerator:number,denominator:number){
      const g= gcd(numerator,denominator);
      this.numerator=numerator/g;
       this.denominator=denominator/g;
     }
     add(that:Rational):Rational{
       const numerator=this.numerator*that.denominator+this.denominator*that.numerator;
       const denominator=this.denominator*that.denominator;
      const result=new Rational(numerator,denominator);
      return result;
    }
    substraction(that:Rational):Rational{
      const numerator=this.numerator*that.denominator-this.denominator*that.numerator;
      const denominator=this.denominator*that.denominator;
      const result=new Rational(numerator,denominator);
      return result;
    }
    division(that:Rational):Rational{
      const numerator=(this.numerator/that.denominator)*(this.denominator/that.numerator);
      const denominator=(this.denominator*that.denominator);
      const result=new Rational(numerator,denominator);
      return result;
    }
    multiplication(that:Rational):Rational{
      const numerator=(this.numerator*that.denominator)*(this.denominator*that.numerator);
      const denominator=(this.denominator*that.denominator);
      const result=new Rational(numerator,denominator);
      return result;
  }
  compare(x:Rational):number{
  const y= this.numerator*x.denominator*this.denominator*x.denominator 
    if(y===0){
  return 0;
    }
    if(y>0){
  return 1;
    }
    if(y<0){
      return -1;
    }
    return y;
  }
  }
  const r1=new Rational(2,2);
  const r2=new Rational(1,4);
  const r3=r1.add(r2);
  const r4=r1.substraction(r2);
  const r5=r1.division(r2);
  const r6=r1.multiplication(r2);
  const r7=r1.compare(r2);
  console.log(r3.numerator,r3.denominator);
   console.log(r4.numerator,r4.denominator);
  console.log(r5.numerator,r5.denominator);
  console.log(r6.numerator,r6.denominator);
  console.log(r7);