const unique=(arr: ReadonlyArray<number>):ReadonlyArray<number>=>{
  const newarr:ReadonlyArray<number> = [arr[0]];
  for (let i=1; i<arr.length; i++) {
     if (arr[i]!==arr[i-1]){ 
     newarr.push(arr[i]);
  }
  }
  return newarr;
  };
  export default unique;