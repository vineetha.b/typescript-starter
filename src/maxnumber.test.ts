import maxnumber from './maxnumber';
test('maxnumber',()=>{
  expect(maxnumber([7,9])).toEqual(9)
  expect(maxnumber([11,9])).toEqual(11)
  expect(maxnumber([7,90])).toEqual(90)
  expect(maxnumber([75,9])).toEqual(75)
  expect(maxnumber([54,9])).toEqual(54)
})