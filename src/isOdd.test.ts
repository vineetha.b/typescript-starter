import isOdd from './isOdd';
test('isOdd',()=>{
  expect(isOdd(5)).toBeFalsy()
  expect(isOdd(5)).toBeTruthy()
  expect(isOdd(51)).toBeTruthy()
  expect(isOdd(98)).toBeFalsy()
  expect(isOdd(2)).toBeFalsy()
})