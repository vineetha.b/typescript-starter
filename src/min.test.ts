import min from './min';
test('min',()=>{
  expect(min(7,9)).toEqual(7)
  expect(min(1,9)).toEqual(1)
  expect(min(7,90)).toEqual(7)
  expect(min(75,9)).toEqual(9)
  expect(min(54,9)).toEqual(9)
})