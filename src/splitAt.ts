const splitAt=(arr: ReadonlyArray<number>, n: number): [ReadonlyArray<number>, ReadonlyArray<number>]=>{
  const ar1:number[]=[];
  for(let i=0;i<n;i++){
ar1.push(arr[i]);
  }
  const ar2:number[]=[];
  for(let i=n;i<arr.length;i++){
    ar2.push(arr[i]);
  }
  return ([ar1,ar2]);
}
export default splitAt;