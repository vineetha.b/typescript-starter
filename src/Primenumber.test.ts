import {Primenumber} from './Primenumber';
test('Primenumber',()=>{
  expect(Primenumber(1,20)).toEqual([2,3,5,7,11,13,17,19])
  expect(Primenumber(1,8)).toEqual([2,3,5,7])
  expect(Primenumber(1,30)).toEqual([2,3,5,7,11,13,17,19,23,29])
  expect(Primenumber(1,2)).toEqual([2])
  expect(Primenumber(1,15)).toEqual([2,3,5,7,11,13])
  
})