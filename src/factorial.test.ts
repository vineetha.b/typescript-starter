import factorial from './factorial';
test('factorial',()=>{
expect(factorial(5)).toEqual(120);
expect(factorial(0)).toEqual(1);
expect(factorial(2)).toEqual(2);
expect(factorial(8)).toEqual(40);
expect(factorial(4)).toEqual(20);
});