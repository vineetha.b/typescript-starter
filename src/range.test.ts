import range from './range';
test('range',()=>{
  expect(range(0,8)).toEqual([0,1,2,3,4,5,6,7,8])
  expect(range(0,5)).toEqual([0,1,2,3,4,5,6,7,8])
})