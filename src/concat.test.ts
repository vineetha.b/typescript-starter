import concat from './concat';
test('concat',()=>{
  expect(concat([1,2],[4,8])).toEqual([1,2,4,8]);
  expect(concat([1,2,5],[4,8])).toEqual([1,2,5,4,8]);
  expect(concat([1,2],[4,8,6])).toEqual([1,2,4,8,6]);
  expect(concat([1,21],[4,8])).toEqual([1,21,4,8]);
  
});
