import unique from './unique';
test('unique',()=>{
  expect(unique([1,1,2,2,3,4])).toEqual([1,2,3,4]);
  expect(unique([1,1,2,3,3,4])).toEqual([1,2,3,4]);
  expect(unique([1,1,2,3,4])).toEqual([1,2,3,4]);
  expect(unique([1,1,1,3,4])).toEqual([1,3,4]);
})