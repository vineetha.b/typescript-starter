import zip from './zip';
test('zip',()=>{
  expect(zip([1,2,3],[2,3,4])).toEqual([[1,2],[2,3],[3,4]])
  expect(zip([1,2,3],[8,7,6])).toEqual([[1,8],[2,7],[3,6]])
  expect(zip([1,2,3],[6,8,9])).toEqual([[1,6],[2,8],[3,9]])
  expect(zip([5,4,6],[2,3,4])).toEqual([[5,2],[4,3],[6,4]])
});