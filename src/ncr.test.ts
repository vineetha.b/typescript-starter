
import ncr from './ncr';
test('ncr',()=>{
  expect(ncr(5,3)).toEqual(10)
  expect(ncr(10,2)).toEqual(20)
  expect(ncr(5,2)).toEqual(10)
  expect(ncr(11,1)).toEqual(11)
})