 
 
 
 const perfect = (x: number)=> {
  let sum = 0;
  for (let i = 1; i<=x/2; i++) {
    if (x % i === 0) {
      sum = sum + i;
    }
  }
  if (sum === x&& sum!==0) {
    return true;
  }
  return false;
}
 
const range = (start: number, stop: number):number[] => {
  const arr:number[] = [];
  for (let i = start; i <= stop; i++) {
    arr.push(i);
  }
  return arr;
};

 const Perfectnumbers=(start:number,stop:number):ReadonlyArray<number>=>range(start,stop).filter(perfect);

 export {perfect,range,Perfectnumbers};